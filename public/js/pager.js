define(

	['jquery'],
	function ($) {

		var Page = function (name) {
			var templates = {}, view,
				options = {
					'baseTplURL': '/js_templates',
					'fileExt': 'jst'
				},
				response = $.ajax({
					url: options.baseTplURL + '/' + name + '.' + options.fileExt,
					type: "GET",
					async: false
				}).responseText,
				tpl_raw_page = $('<div/>').html(response),

				Tpl = function (id) {
					if (!templates[id]) {
						templates[id] = tpl_raw_page.find('#' + id).html();
					}
					return templates[id];
				};

			Tpl.name = name;

			Tpl.view = function (value) {
				if (value) {
					view = value;
				}
				return view;
			};

			Tpl.setOptions = function (hash) {
				$.extend(options, hash);
				return this;
			};

			return Tpl;
		};

		var Pager = function () {
			var pages = {},
				options = {};

			return {

				page: function (name, view) {
					if (view) {
						pages[name] = new Page(name);
						pages[name].setOptions(options).view(view);
					}
					return pages[name]
				},

				views: function () {
					var views = [];
					$.each(pages, function(key, el) {
						if (typeof el.view() != 'function') {
							views.push(el.view());
						}
					});
					return views;
				}
			};
		};

		return new Pager();
	}
);