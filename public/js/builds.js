define(

	['jquery', 'underscore', 'backbone', '/js/app.js'],

	function ($, _, Backbone, App) {

		var CommitsModel = Backbone.Model.extend({

		});

		var CommitsCollection = Backbone.Collection.extend({
			url: '/api/builds_commits',
			model: CommitsModel
		});

		var CommitsTableRowView = Backbone.View.extend({
			tagName: 'tr',
			rowTplName: 'template_builds_commit_table_row',

			render: function () {
				this.$el.html(_.template(App.Pager.page('builds')(this.rowTplName), this.model.toJSON()));
				return this.el;
			}

		});

		var CommitsTableView = Backbone.View.extend({
			tagName: 'table',
			className: 'table table-striped table-hover',
			collection: new CommitsCollection,
			titles: {titles: ['Rev', 'Author', 'Date', 'Commit message']},
			rowTemplate: CommitsTableRowView,

			initialize: function () {
				this.$el.html(_.template(App.Pager.page('builds')('template_builds_commit_table_head'), this.titles));

				this.listenTo(this.collection, 'add', this.addOne.bind(this));
				this.listenTo(this.collection, 'reset', this.addAll.bind(this));
				this.collection.fetch();
			},

			render: function () {
				return this.el;
			},

			addOne: function (model) {
				var view = new this.rowTemplate({model: model});
				this.$el.find('.commits_rows').append(view.render());
			},

			addAll: function () {
				this.$el.find('.commits_rows').empty();
				this.collection.each(this.addOne.bind(this));
			}
		});



		var TagsCollection = CommitsCollection.extend({
			url: '/api/builds_tags'
		});

		var TagsTableRowView = CommitsTableRowView.extend({
			rowTplName: 'template_builds_tag_table_row'
		});

		var TagsTableView = CommitsTableView.extend({
			titles: {titles: ['Rev', 'Author', 'Size', 'Date', 'Name', 'Type']},
			collection: new TagsCollection(),
			rowTemplate: TagsTableRowView
		});




		return Backbone.View.extend({

			events: {
				'click a[role=tab]': 'switchTab'
			},

			pages: {},

			initialize: function () {
				this.$el.html(App.Pager.page('builds')('template_app_builds'));
				var that = this;
				setTimeout(function () {
					that.$el.find('li.active a').click();
				}, 100);
			},

			switchTab: function (e) {
				var tab_id = $(e.target).attr('href'),
					tab_name = tab_id.replace(/#/, ''),
					views = {
						'commits': CommitsTableView,
						'tags': TagsTableView,
						'builds': Backbone.View
					};

				if (!this.pages[tab_name]) {
					this.pages[tab_name] = new views[tab_name];
					this.$el.find(tab_id).append(this.pages[tab_name].el);
				}

			},

			render: function () {
				return this.el;
			}

		});
	}
);