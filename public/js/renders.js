define(

	['jquery', 'underscore', 'backbone', '/js/app.js'],

	function ($, _, Backbone, App) {

		var RenderModel = Backbone.Model.extend({

			getQuerySting: function (suite_id) {
				return 'suite=' + this.attributes['suites'][suite_id] + '&site=' + this.attributes['name'];
			}
		});

		var RenderCollection = Backbone.Collection.extend({

			url: '/api/render',
			model: RenderModel

		});


		var RenderView = Backbone.View.extend({

			tagName: 'div',
			className: 'panel panel-default',

			events: {
				"click .panel-body .btn": "make"
			},

			initialize: function (params) {
				this.listenTo(this.model, "change", this.render);
				this.process_view = params.process_view;
			},

			render: function () {
				var data = this.model.toJSON();
				data.site_id = data.name.replace(new RegExp('\\.', 'g'), '');
				this.$el.html(_.template(App.Pager.page('renders')('template_render_view'), data));
				this.delegateEvents();
				return this;
			},

			make: function (el) {
				var suite_id = el.currentTarget.getAttribute('data-suite'),
					query = this.model.getQuerySting(suite_id);
				if (this.model.attributes['suites'][suite_id] == 'test') {
					$('#popup_placeholder').html(
						_.template($('#template_modal_popup').html(), {title: 'Enter render', content: '<label><span>Render name: </span><input name="render"></label>'
						})
					);
					$('#mainModal').modal().find('.modal-footer button.btn-primary').on('click', function (e) {
						e.preventDefault();
						query += '&render=' + encodeURIComponent(
							$('#mainModal').modal('hide').find('input[name=render]').val()
						);
						this.process_view.make(query);
					}.bind(this));
				} else {
//					alert(query);
					this.process_view.make(query);
				}
			}
		});

		var RenderProcessView = Backbone.View.extend({

			alert: null,
			timer: null,

			initialize: function (MainView) {
				this.$el = MainView.find('#render_process');

				this.alert = function () {
					MainView.find('#render_process_message').html(App.Pager.page('renders')('template_alert_success'));
					//if (this.timer) {
						clearInterval(this.timer);
					//}
				}
			},

			make: function (query) {
				this.$el.html(_.template(App.Pager.page('renders')('template_process_view'), {query: query}));
				this.$el.find('#process_iframe').load(this.alert.bind(this));
				var $el = this.$el;
				this.timer = setInterval(function() {
					var $contents = $el.find('#process_iframe').contents();
						$contents.scrollTop($contents.height());
				}, 1000);
			}

		});

		return Backbone.View.extend({

			initialize: function () {

				this.$el.html(App.Pager.page('renders')('template_app_renders'));
				this.Process = new RenderProcessView(this.$el);

				this.collection = new RenderCollection;

				this.listenTo(this.collection, 'add', this.addOne.bind(this));
				this.listenTo(this.collection, 'reset', this.addAll.bind(this));

				this.collection.fetch();
			},

			init: function () {

			},
			addOne: function (model) {
				var view = new RenderView({model: model, process_view: this.Process});
				this.$el.find('#accordion').append(view.render().el);
			},

			addAll: function () {
				this.collection.each(this.addOne.bind(this));
			},

			render: function () {
				return this.el;
			}
		});
	}
);