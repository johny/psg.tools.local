define(

	['jquery', 'underscore', 'backbone', '/js/pager.js'],

	function ($, _, Backbone, Pager) {

		window.$ = $;

		Backbone.emulateHTTP = 1;
		Backbone.$ = $;

		var prev_url;
		var App;

		var AppView = Backbone.View.extend({
			Menu: null,
			Router: null,
			Pager: null,
			el: $('div#container'),

			initialize: function() {
				this.$el.empty();
			}
		});

		App = new AppView();

		var MenuView = Backbone.View.extend({
			el: $('#navbar_menu'),

			initialize: function () {
				this.$el.find('li').tooltip();
			},

			render: function (urls) {
				this.$el.find('li').each(function (key, val) {
					var el = $(val), href;

					el.removeClass('active');
					if (el.children('a')[0]) {
						href = el.children('a')[0].getAttribute('href');
						if ($.inArray(href, urls) >= 0) {
							el.addClass('active');
						}
					}
				});
				return this;
			}
		});

		App.Menu = new MenuView();
		App.Pager = Pager;

		var MainRouter = Backbone.Router.extend({
			routes: {
					':section': function (section) {
					this.redirect({module: section, urls: ['/' + section]});
				},

				':section/:subsection': function (section, subsection) {
					this.redirect({module: section + '/' + subsection, urls: ['#' + section, '/' + section + '/' + subsection]});
				}
			},

			redirect: function (params) {
				var name = params.module.replace(/\//, '_');

				function showPage() {
					_.each(App.Pager.views(), function(view) {
						view.$el.hide();
					});
					App.Pager.page(name).view().$el.show();
					App.Menu.render(params.urls);
				}

				if (!App.Pager.page(name)) {
					require(
						['/js/' + params.module + '.js'],
						function (view) {
							App.Pager.page(name, view);
							App.Pager.page(name).view(new view);
							App.$el.append(App.Pager.page(name).view().el);
							showPage();
						},
						function (err) {
							App.Router.navigate(prev_url, {trigger: true});
							alert('An error occurs while loading module "' + name + '"');
							console.log(err);
						}
					);
				} else {
					showPage();
				}
			}
		});

		App.Router = new MainRouter();
		Backbone.history.start({pushState: true});

		$('body').find('a').each(function (key, value) {
			var el = $(value);
			el.on('click', function (e) {
				e.preventDefault();
				var href = el[0].getAttribute('href');
				if (href.indexOf('#') < 0) {
					prev_url = location.pathname + location.search;
					App.Router.navigate(el[0].getAttribute('href'), {trigger: true});
				}
			})
		});
		return App;
	}
);
