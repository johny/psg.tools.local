define(

	['jquery', 'underscore', 'backbone', '/js/app.js', 'simplePagination'],

	function ($, _, Backbone, App) {

		var LogsMailModel = Backbone.Model.extend({

			is_loaded: false,

			is_selected: false,

			defaults: {
				To: '',
				From: '',
				Subject: '< Error while parsing data >',
				Body: ''
			},

			select: function() {
				this.collection.each(function(el) {
					el.is_selected = false;
					el.trigger('unselected');
				});
				this.is_selected = true;
				this.trigger('selected');
			}
		});

		var LogsMailCollection = Backbone.Collection.extend({

			url: '/api/logs_mail',
			model: LogsMailModel

		});

		var LogsMailView = Backbone.View.extend({

			initialize: function(){
				this.listenTo(this.model, 'change', this.render.bind(this));
				if (!this.model.is_loaded) {
					this.model.fetch();
					this.model.is_loaded = true;
				}
			},

			render: function(){
				this.$el.html(_.template(App.Pager.page('logs_mail')('template_logs_mail'), this.model.toJSON()));
				return this;
			}
		});

		var LogsMailTitleView = Backbone.View.extend({

			tagName: 'span',

			events: {
				"click a": "showMail"
			},

			render: function(){
				this.$el.html(_.template(App.Pager.page('logs_mail')('template_logs_mail_title'), this.model.toJSON()));
				this.listenTo(this.model, 'selected unselected', this.updateSelected);
				this.updateSelected();
				return this;
			},

			updateSelected: function() {
				if (this.model.is_selected) {
					this.$el.find('a').removeClass('label-info').addClass('label-default');
				} else {
					this.$el.find('a').removeClass('label-default').addClass('label-info');
				}
			},

			showMail: function(e){
				e.preventDefault();
				this.model.select();
				var mail = new LogsMailView({model: this.model});
				$('#logs_mail').html(mail.render().el);
			}

		});

		var options = {
			itemsOnPage: 20
		};

		return Backbone.View.extend({

			shown_collection: null,
			pagination: null,

			initialize: function () {

				this.$el.html(App.Pager.page('logs_mail')('template_app_logs_mail'));
				this.pagination = this.$el.find('#logs_mail_titles_pagination').pagination({
					itemsOnPage: options.itemsOnPage,
//					displayedPages: 3,
//					edges: 1,
					onPageClick: this.showPage.bind(this)
				});

				this.collection = new LogsMailCollection();
				this.listenTo(this.collection, 'add', this.addOne.bind(this));
				this.listenTo(this.collection, 'reset', this.addAll.bind(this));

				var shown_collection = new LogsMailCollection();
				this.shown_collection = shown_collection;
				this.listenTo(this.shown_collection, 'add', this.showOne.bind(this));
				this.listenTo(this.shown_collection, 'reset', this.showAll.bind(this));

				this.collection.fetch({success: function(collection){
					shown_collection.reset(collection.slice(0, options.itemsOnPage));
				}});

				var that = this;
				setTimeout(function () {
					that.$el.find('#logs_mail_titles span:first-child a').click();
				}, 100);

			},

			showPage: function (number, event) {
				event.preventDefault();
				var start = (number - 1) * options.itemsOnPage;
				this.shown_collection.reset(this.collection.slice(start, start + options.itemsOnPage));
			},

			showOne: function (model) {
				var view = new LogsMailTitleView({model: model});
				this.$el.find('#logs_mail_titles').append(view.render().el);
			},

			showAll: function () {
				this.$el.find('#logs_mail_titles').empty();
				this.shown_collection.each(this.showOne.bind(this));
			},

			addOne: function (model) {
				this.pagination.pagination('updateItems', this.collection.length);
				if (this.pagination.pagination('getPagesCount') > 1) {
					this.pagination.show();
				}
			},

			addAll: function () {
				this.collection.each(this.addOne.bind(this));
			},

			render: function () {
				return this.el;
			}
		});
	}
);