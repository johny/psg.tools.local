<div class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/renders">PSG Tools</a>
		</div>
		<div id="navbar_menu" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active" data-toggle="tooltip" data-placement="bottom" title=""><a href="/renders"><span class="glyphicon glyphicon-compressed"></span> Renders</a></li>
				<li data-toggle="tooltip" data-placement="bottom" title=""><a href="/builds"><span class="glyphicon glyphicon-wrench"></span> Builds</a></li>
				<li class="dropdown">
					<a href="#logs" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list-alt"></span> Logs <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li data-toggle="tooltip" data-placement="bottom" title=""><a href="/logs/mail"><span class="glyphicon glyphicon-envelope"></span> Mail</a></li>
						<li class="divider"></li>
						<li class="dropdown-header"> Apache</li>
						<li data-toggle="tooltip" data-placement="bottom" title=""><a href="/logs/access"><span class="glyphicon glyphicon-ok-circle"></span> Access log</a></li>
						<li data-toggle="tooltip" data-placement="bottom" title=""><a href="/logs/errors"><span class="glyphicon glyphicon-remove-circle"></span> Errors log</a></li>
					</ul>
				</li>
			</ul>
<!--			<ul class="nav navbar-nav navbar-right">-->
<!--				<li><a href="../navbar/">Default</a></li>-->
<!--				<li class="active"><a href="./">Static top</a></li>-->
<!--				<li><a href="../navbar-fixed-top/">Fixed top</a></li>-->
<!--			</ul>-->
		</div><!--/.nav-collapse -->
	</div>
</div>
