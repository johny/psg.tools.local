<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Tools, ProSteerGroup</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

<!--	<link href="/components/require.css" rel="stylesheet">-->
	<link href="/components/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="/components/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
	<style type="text/css">
		html, body {
			height: 100%;
		}

		#wrap {
			min-height: 100%;
			height: auto;
			/* Negative indent footer by its height */
			margin: 0 auto -40px;
			/* Pad bottom by footer height */
			padding: 60px 0 50px;
		}

		/* Set the fixed height of the footer here */
		#footer {
			height: 40px;
			background-color: #f5f5f5;
		}
		/*body {*/
		/*padding-top: 60px;*/
			/*padding-bottom: 40px;*/
		/*}*/
	</style>

</head>
<body data-spy="scroll" data-target="#container">
<div id="wrap">
	<?php include_once 'menu.tpl'; ?>

	<div class="container" id="container">

		<div class="row-fluid">
			<div class="col-md-4">
				<h2>Loading...</h2>
			</div>

		</div>
	</div>
	<div id="popup_placeholder"></div>
</div>
<div id="footer">
	<div class="container">
		<footer>
		<p>ProSteerGroup &copy; 2013&ndash;<?php echo date('Y'); ?></p>
		</footer>
	</div>
</div>

<?php include_once 'templates/modal.tpl'; ?>

<script data-main="/js/main.js" src="/components/require-built.js"></script>
</body>
</html>

