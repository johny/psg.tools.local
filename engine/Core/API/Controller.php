<?php
/**
 * File: Controller.php
 * User: I.Volkov
 * Date: 07.12.13
 * Time: 12:25
 */
/**
 * Class Controller
 * @package API
 */


namespace Core\API;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Controller
{

	/**
	 * @param $collection
	 * @param Application $app
	 * @return \Core\Base\Module
	 * @throws \ErrorException
	 */
	protected static function _getModule($collection, Application $app)
	{
		$collection = implode('\\', array_map('ucfirst', explode('_', $collection)));
		if (!class_exists($class_name = 'Modules\\' . ucfirst($collection) . '\Module')) {
			throw new \ErrorException('Module ' . $class_name . ' doesn\'t exists');
		}
		return new $class_name($app);
	}

	public static function getCollection($collection, Request $request, Application $app)
	{
		$app->is_json = true;
		$result = null;
		if ($module = self::_getModule($collection, $app)) {
			$result = $module->getRecords();
		}
		return $app->json($result);
	}

	public function getItem($collection, $id, Request $request, Application $app)
	{
		$app->is_json = true;
		$result = null;
		if ($module = self::_getModule($collection, $app)) {
			$result = $module->getRecord($id);
		}
		return $app->json($result);
	}
} 