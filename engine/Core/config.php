<?php
/**
 * File: config.php
 * User: I.Volkov
 * Date: 04.12.13
 * Time: 11:14
 */
/**
 * Class Config
 */


class Config {

	protected static $_config = null;

	protected static function _load(array $dirs = array(DIR_FS_CONFIG))
	{
		$env_dirs = array();

		foreach ($dirs as $dir) {
			$env_dirs[] = $dir . DIRECTORY_SEPARATOR . APP_ENV;
		}
		$dirs = array_merge($dirs, $env_dirs);

		static::$_config = array();

		// load ini files
		foreach ($dirs as $dir) {
			if (file_exists($filename = $dir . DIRECTORY_SEPARATOR . 'engine.ini')) {
				static::$_config = array_merge(static::$_config, parse_ini_file($filename, true));
			}
		}

		// load php files
		foreach ($dirs as $dir) {
			if (file_exists($filename = $dir . DIRECTORY_SEPARATOR . 'engine.php')) {
				static::$_config = array_merge(static::$_config, require_once($filename));
			}
		}
	}

	/**
	 * @return array
	 */
	public static function getConfig()
	{
		if (static::$_config === null) {
			static::_load();
		}
		return static::$_config;
	}
}