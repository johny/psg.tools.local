<?php
/**
 * File: Controller.php
 * User: I.Volkov
 * Date: 29.11.13
 * Time: 18:21
 */
/**
 * Class Controller
 * @package Tools
 */


namespace Core\Tools;

use Core\Base;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Controller extends  Base\Controller {


	public function run($module, Application $app, Request $request)
	{
		$app->is_json = true;

		if (
			class_exists($class = 'Modules\\' . ucfirst($module) . '\\Module')
			and method_exists($class, 'run')
		) {
			$module = new $class($app);
			$module->run($request);
		}
		return ' ';
	}
} 