<?php
/**
 * File: functions.php
 * User: i.volkov
 * Date: 16.12.13
 * Time: 16:10
 */

function scanDirForFiles($dir, $remove_ext = false)
{
	$result = array();
	$old_dir = getcwd();
	chdir($dir);
	foreach (glob('*') as $file) {
		if ($file !== '.' and $file !== '..') {
			if (is_dir($file)) {
				$result[$file] = scanDirForFiles($file, $remove_ext);
			} else if (is_file($file)) {
				if ($remove_ext) {
					$path_parts = pathinfo($file);
					$result[] = $path_parts['filename'];
				} else {
					$result[] = $file;
				}
			}
		}
	}
	chdir($old_dir);
	return $result;
}
