<?php
/**
 * File: constants.php
 * User: I.Volkov
 * Date: 27.11.13
 * Time: 19:21
 */

if (empty($_SERVER['APP_ENV'])) {
	exit ('Couldn\'t continue working without environment constant APP_ENV set. '
		. 'Check web server virtual host configuration');
}

define('APP_ENV', $_SERVER['APP_ENV']);

define('DIR_FS_ROOT', dirname(dirname(__DIR__)));
define('DIR_FS_CONFIG', DIR_FS_ROOT . '/config');
define('DIR_FS_ENGINE', DIR_FS_ROOT . '/engine');
define('DIR_FS_VENDOR', DIR_FS_ROOT . '/vendor');
define('DIR_FS_TOOLS', DIR_FS_ROOT . '/tools');
define('DIR_FS_VIEWS', DIR_FS_ROOT . '/view');