<?php
/**
 * File: Controller.php
 * User: I.Volkov
 * Date: 29.11.13
 * Time: 18:03
 */
/**
 * Class Controller
 * @package Main
 */

namespace Core\Main;

use Core\Base;

class Controller extends Base\Controller
{
	protected $_module = __NAMESPACE__;

}