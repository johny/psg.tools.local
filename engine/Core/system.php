<?php

require_once 'constants.php';
require_once 'functions.php';
require_once 'config.php';

$auto_loader = require_once DIR_FS_VENDOR . '/autoload.php';

$auto_loader->add('', dirname(__DIR__));

$app = require_once 'app.php';

if (empty($app['system.config']['psg']['dir'])) {
	exit('Engine dir isn\'t set');
}

if (is_file($filename = $app['system.config']['psg']['dir'] . DIRECTORY_SEPARATOR . 'Library/initialization.php')) {
	require_once $filename;
}
