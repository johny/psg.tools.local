<?php
/**
 * File: Module.php
 * User: I.Volkov
 * Date: 04.12.13
 * Time: 12:48
 */
/**
 * Class Module
 * @package Base
 */


namespace Core\Base;

use Silex\Application;

class Module
{
	protected $_app;

	function __construct(Application $app)
	{
		$this->_app = $app;
	}

	public function getRecord($id)
	{
		return $id;
	}

	public function getRecords()
	{
		return array();
	}
}