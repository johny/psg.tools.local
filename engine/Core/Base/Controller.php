<?php
/**
 * File: Controller.php
 * User: I.Volkov
 * Date: 29.11.13
 * Time: 18:22
 */
/**
 * Class Controller
 * @package Base
 */

namespace Core\Base;

use Silex;

class Controller
{

	protected $_module = __NAMESPACE__;
	protected $_view_dirs = array(DIR_FS_VIEWS);
	protected $_view = 'body';
	protected $_view_file_ext = '.tpl';

	public static function getResponse()
	{
		$main = new static;
		return $main->render();
	}

	public function __construct()
	{
		array_unshift($this->_view_dirs, DIR_FS_VIEWS . '/' . str_replace('\\', '/', strtolower($this->_module)));
	}

	public function render()
	{
		$body = ' ';
		foreach ($this->_view_dirs as $dir) {
			$filename = $dir . DIRECTORY_SEPARATOR . $this->_view . $this->_view_file_ext;
			if (file_exists($filename)) {
				ob_start();
				include_once($filename);
				$body = ob_get_clean();
				break;
			}
		}
		return $body;
	}

} 