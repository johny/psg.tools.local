<?php

$app = new Silex\Application();

$app->get('/', function () use ($app) { return $app->redirect('/renders'); });
$app->get('/api/{collection}', '\\Core\\API\\Controller::getCollection');
$app->get('/api/{collection}/{id}', '\\Core\\API\\Controller::getItem');
//$app->get('/tools/{module}', function ($module) {
//		$module = '\\Tools\\' . ucfirst($module) . '\\Controller';
//		$module = new $module();
//		$module->make();
//	});
$app->get('/tools/{module}', '\\Core\\Tools\\Controller::run');
//$app->get('/tools/{collection}/{id}', 'API\\Controller::getItem');
$app->get('/{something}', array('\\Core\\Main\\Controller', 'getResponse'));
$app->get('/{something}/{something_else}', array('\\Core\\Main\\Controller', 'getResponse'));

$app['system.config'] = $app->share(function() {
	return \Config::getConfig();
});

return $app;