<?php
/**
 * File: Module.php
 * User: i.volkov
 * Date: 16.12.13
 * Time: 10:54
 */

namespace Modules\Logs\Mail;

use Core\Base;
use Silex\Application;

class Module extends Base\Module
{

	const FILE_EXT = 'txt';

	protected $files = array();

	public function __construct(Application $app)
	{
		parent::__construct($app);
		$this->_getFiles();
	}

	protected function _getFiles()
	{
		$dir = (empty($this->_app['system.config']['Mail']['log_dir']))
			? $this->_app['system.config']['Mail']['log_dir']
			: $this->_app['system.config']['engine']['dir'] . DIRECTORY_SEPARATOR . 'Logs'
				. DIRECTORY_SEPARATOR . 'Mail';

		if (file_exists($dir)) {
			foreach (scanDirForFiles($dir, self::FILE_EXT) as $file_name) {
				if (is_file($file = $dir . DIRECTORY_SEPARATOR . $file_name . '.' . self::FILE_EXT)) {
					$this->files[$file_name] = $file;
				}
			}
		}
	}

	public function getRecords()
	{
		return array_map(function($e) { return array('id' => $e); }, array_keys($this->files));
	}

	public function getRecord($id)
	{
		$result = 'File not found';
		if (is_file($file = $this->files[$id]) and $data = file_get_contents($file)) {
			$result = array();
			if (($del_pos = (strpos($data, PHP_EOL . PHP_EOL))) === false) {
				$result['Body'] = trim($data);
			} else {
				$headers = substr($data, 0, $del_pos);
				$body = substr($data, $del_pos + 1);
				foreach (explode(PHP_EOL, $headers) as $row) {
					if (($del_pos = strpos($row, ':')) !== false) {
						$result[substr($row, 0, $del_pos)] = trim(substr($row, $del_pos + 1));
					} else {
						$result[] = trim($row);
					}
				}
				$result['Body'] = trim($body);
			}
		}
		return $result;
	}

}