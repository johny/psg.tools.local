<?php
/**
 * File: Module.php
 * User: I.Volkov
 * Date: 07.12.13
 * Time: 19:41
 */
/**
 * Class Module
 * @package Render
 */


namespace Modules\Render;

use Core\Base;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Module extends Base\Module
{

	protected $_renderers_dir;

	function __construct(Application $app)
	{
		parent::__construct($app);

		if (empty($this->_app['system.config']['Renderer']['dir'])) {
			throw new \Exception('Renderers dir not set in config!');
		}
		$this->_renderers_dir = $this->_app['system.config']['Renderer']['dir'];
	}


	public function getRecords()
	{
		$result = $array = null;
		if (file_exists($dir = $this->_renderers_dir . '/Suites')) {
			$array = scanDirForFiles($dir, true);
		}
		foreach ($array as $name => $value) {
			$process = ($pid_data = $this->_checkPid($name)) ? 'Suite "' . $pid_data['suite'] . '" still running...' : '';
			$result[] = array('name' => $name, 'suites' => $value, 'process' => $process);
		}
		return $result;
	}

	public function run(Request $request)
	{
		if ($site = $request->query->get('site') and $suite = $request->query->get('suite')) {
			if ($pid_data = $this->_checkPid($site)) {
				echo "<p>Another process with PId <b>{$pid_data['pid']}</b> still running. It's run "
					. "\"<b>{$pid_data['suite']}</b>\" suite on <b>{$pid_data['site']}</b> site.</p>"
					. "<p></p>";
			} else {
				$argv = array("'this item will be deleted'", "site=$site", "suite=$suite");
				if ($render = $request->query->get('render')) {
					$argv[] = "render=$render";
				}
				$argc = count($argv);
				if (is_file($file = $this->_renderers_dir . '/index.php')) {
					set_time_limit(0);
					ignore_user_abort(true);
					include $file;
				}
			}
		} else {
			echo "Invalid params";
		}
	}

	protected function _checkPid($site)
	{
		$result = false;
		if (is_file($pid_file = $this->_renderers_dir . '/render.' . $site .'.pid')) {
			$result = file_get_contents($pid_file);
			list($pid, $site, $suite) = explode(PHP_EOL, $result);
			$result = compact('pid', 'site', 'suite');
		}
		return $result;
	}
} 