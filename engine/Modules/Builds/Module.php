<?php
/**
 * File: Module.php
 * User: I.Volkov
 * Date: 05.08.14
 * Time: 13:41
 */

/**
 * Class Module
 * @package Modules\Builds
 */


namespace Modules\Builds;

use Core\Base;
use Silex\Application;

class Module extends Base\Module
{

} 