<?php
/**
 * File: SVN.php
 * User: I.Volkov
 * Date: 05.08.14
 * Time: 14:03
 */

/**
 * Class SVN
 * @package Modules\Builds
 */


namespace Modules\Builds;

use Core\Base;
use Silex\Application;

class SVN extends Base\Module
{
	public function auth() {
		$user = !empty($this->_app['system.config']['svn']['user']) ? $this->_app['system.config']['svn']['user']
			: 'user';
		$pass = !empty($this->_app['system.config']['svn']['pass']) ? $this->_app['system.config']['svn']['pass']
			: 'pass';

		svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_USERNAME, $user);
		svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_PASSWORD, $pass);

	}
} 