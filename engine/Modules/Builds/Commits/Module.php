<?php
/**
 * File: Module.php
 * User: I.Volkov
 * Date: 05.08.14
 * Time: 14:02
 */

/**
 * Class Module
 * @package Modules\Builds\Commits
 */


namespace Modules\Builds\Commits;

use Silex\Application;
use Modules\Builds\SVN;

class Module extends SVN
{
	public function getRecords()
	{
		$result =  array(
			array('id' => 1),
			array('id' => 2),
		);

		try {

			$this->auth();

			$project_dir = !empty($this->_app['system.config']['engine']['dir']) ? $this->_app['system.config']['engine']['dir'] : './';

			$result = svn_log($project_dir, SVN_REVISION_HEAD, SVN_REVISION_INITIAL, 20);
			foreach ($result as &$value) {
				$date = new \DateTime($value['date']);
				$date->setTimezone(new \DateTimeZone('Europe/Kiev'));
				$value['date'] = $date->format('Y-m-d H:i:s');
			}

		} catch (\Exception $e) {

		}

		return $result;
	}


} 