<?php
/**
 * File: Module.php
 * User: I.Volkov
 * Date: 05.08.14
 * Time: 14:05
 */

/**
 * Class Module
 * @package Modules\Builds\Tags
 */


namespace Modules\Builds\Tags;

use Silex\Application;
use Modules\Builds\SVN;

class Module extends SVN
{
	public function getRecords()
	{
		$result =  array(
			array('id' => 1),
			array('id' => 2),
		);

		$this->auth();

		try {
			$result = svn_ls('http://gforge/svn/psgsites_mongo/tags');
			$result = array_values($result);
			foreach ($result as &$value) {
				$value['time'] = date('Y-m-d H:i:s', $value['time_t']);
			}

		} catch (\Exception $e) {

		}

		return $result;
	}

} 